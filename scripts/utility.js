/* Connect and subscribe to RABBITMQ */
let rabbitMQHostDomain = localStorage.getItem('rabbitmq_host_domain');
let rabbitMQWebStompPort = localStorage.getItem('rabbitmq_webstomp_port');
let rabbitMQUserName = localStorage.getItem('rabbitmq_username');
let rabbitMQPassword = localStorage.getItem('rabbitmq_password');
let rabbitMQWebSocket = rabbitMQHostDomain + ':' + rabbitMQWebStompPort + '/stomp';
let rabbitMQClient = connectRabbitMq(rabbitMQWebSocket, rabbitMQUserName, rabbitMQPassword);

function connectRabbitMq (socket, username, password) {
    let ws = new SockJS(socket);
    let client = Stomp.over(ws);
    // RabbitMQ SockJS does not support heart-beats
    client.heartbeat.outgoing = 0;
    client.heartbeat.incoming = 0;
    client.debug = onDebug;

    client.connect(username, password, '', onError, '/');
    return client;

    function onError(e) {
        console.log("STOMP ERROR", e);
    }

    function onDebug(m) {
        //console.log("STOMP DEBUG", m);
    }
}

/**
 * @return {boolean}
 */
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
let getBOEStatus = function(rabbitMQClient, boeDeactivateExchangeName, routingKey) {
    let boe_deactivate_exchange_id = rabbitMQClient.subscribe("/exchange/"+boeDeactivateExchangeName+"/"+routingKey, function(d) {
        if(IsJsonString(d.body)) {
            let message = JSON.parse(d.body);
            if(message.status === 'deactivated') {
                alert('Your account was deactivated.Please contact administrator');
                localStorage.removeItem('isLoggedIn');
                localStorage.removeItem('token');
                window.location = '/';
                /*Change this to paper-dialog TODO*/
                // swal({
                //         title: "Your account was deactivated.Please contact administrator",
                //         text: "",
                //         type: "error",
                //         showCancelButton: false,
                //         closeOnConfirm: false,
                //         showLoaderOnConfirm: false,
                //     },
                //     function(){
                //         window.location.href = "../index.php";
                //     });
            }
        }
    });
};
